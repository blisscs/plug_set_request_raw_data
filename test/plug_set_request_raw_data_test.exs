defmodule PlugSetRequestRawDataTest do
  use ExUnit.Case, async: true
  use Plug.Test

  defmodule Check do
    def check_true(%Plug.Conn{}) do
      true
    end

    def check_false(%Plug.Conn{}) do
      false
    end
  end

  defmodule MyBuilderTrue do
    use Plug.Builder

    alias Plug.Parsers
    alias PlugSetRequestRawData

    plug(PlugSetRequestRawData, %{check: &Check.check_true/1})

    plug(
      Parsers,
      parsers: [:urlencoded, :multipart, :json],
      pass: ["*/*"],
      json_decoder: Poison
    )
  end

  defmodule MyBuilderFalse do
    use Plug.Builder

    alias Plug.Parsers
    alias PlugSetRequestRawData

    plug(PlugSetRequestRawData, %{check: &Check.check_false/1})

    plug(
      Parsers,
      parsers: [:urlencoded, :multipart, :json],
      pass: ["*/*"],
      json_decoder: Poison
    )
  end

  test "plug PlugSetRequestRawData should set conn.private[:raw_request_data] when check is true" do
    body = """
    {"a": "a"}

    """

    conn =
      conn(:get, "/", body)
      |> put_req_header("accept", "application/json")
      |> MyBuilderTrue.call(%{})

    assert conn.private.raw_request_data == body
  end

  test "plug PlugSetRequestRawData should not set conn.private[:raw_request_data] when check is false" do
    body = """
    {"a": "a"}
    """

    conn =
      conn(:get, "/", body)
      |> put_req_header("accept", "application/json")

    MyBuilderFalse.call(conn, %{})

    assert conn.private[:raw_request_data] == nil
  end
end
