defmodule PlugSetRequestRawData.MixProject do
  use Mix.Project

  @version "0.1.1"

  def project do
    [
      app: :plug_set_request_raw_data,
      version: @version,
      elixir: "~> 1.4",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      name: "PlugSetRequestRawData",
      docs: docs()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:plug, "~> 1.6.0"},
      {:ex_doc, "~> 0.18", only: :dev},
      {:excoveralls, "~> 0.8", only: :test},
      {:credo, "~> 0.9.3", only: [:dev, :test], runtime: false}
    ]
  end

  def version do
    @version
  end

  defp package do
    [
      name: "plug_set_request_raw_data",
      maintainers: [
        "Suracheth Chawla"
      ],
      description: "Set request rawdata in plug",
      licenses: ["MIT"],
      links: %{gitlab: "https://gitlab.com/blisscs/plug_set_request_raw_data"}
    ]
  end

  defp docs do
    [main: "PlugSetRequestRawData"]
  end
end
