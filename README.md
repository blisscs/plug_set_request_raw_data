# PlugSetRequestRawData

[![pipeline status](https://gitlab.com/blisscs/plug_set_request_raw_data/badges/master/pipeline.svg)](https://gitlab.com/blisscs/plug_set_request_raw_data/commits/master)
[![coverage report](https://gitlab.com/blisscs/plug_set_request_raw_data/badges/master/coverage.svg)](https://gitlab.com/blisscs/plug_set_request_raw_data/commits/master)


This plug will set raw request data in `conn.private[:raw_request_data]` where conn is of type `%Plug.Conn{}`

## Usage and Installation Instruction

Checkout usage and installation instruction in https://hexdocs.pm/plug_set_request_raw_data

## License

`PlugSetRequestRawData` is licensed under MIT License.
