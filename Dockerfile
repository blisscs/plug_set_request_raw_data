FROM elixir:1.6.6
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mkdir -p /plug_set_request_raw_data
ADD . /plug_set_request_raw_data
WORKDIR /plug_set_request_raw_data
